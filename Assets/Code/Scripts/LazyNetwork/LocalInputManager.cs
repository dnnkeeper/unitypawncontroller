﻿using System;
using UnityEngine;
using System.Collections;
#if CROSS_PLATFORM_INPUT
using UnityStandardAssets.CrossPlatformInput;
#endif

namespace LazyNetwork
{
    public class LocalInputManager : Singleton<LocalInputManager>, IControls
    {
        public static ControllerState currentControls
        {
            get { return Instance._controls;  }
        }

		protected bool bInitialized, bInitReported;

		protected ControllerState _controls;
		public ControllerState controls
		{
			get { return _controls;  }
		}
             
        [ReadOnly]
        public Vector3 _moveDir;
        public static Vector3 moveDir
        {
            get
            {
                return Instance._moveDir;
            }
        }

        [ReadOnly]
        public float _moveSpeed;
        public static float moveSpeed
        {
            get
            {
                return Instance._moveSpeed;
            }
        }

        public void Init(Quaternion rot)
        {
			_controls = new ControllerState(Vector3.zero, Quaternion.identity, false, false, false, false, false, false, false, false, false, true, 0);
            bInitialized = true;
            Debug.Log("<b>[LocalInputManager] <color=green>initialized succesfully</color></b>");
            _lookRot = rot;
        }

        [ReadOnly]
        public Vector3 _lookDir;
        protected Quaternion _lookRot = Quaternion.identity;
        public static Quaternion lookRot
        {
            get
            {
                return Instance._lookRot;
            }
        }

        void Update()
        {
            if (!bInitialized)
            {
                //get any camera to set default rotation
                Camera cam = Camera.main;
                if (cam == null)
                {
                    if (!bInitReported)
                    {
                        bInitReported = true;
                        Debug.LogWarning("<color=yellow>No Camera.main found. <b>[LocalInputManager]</b> awaits for main camera</color>");
                    }
                    return;
                }
                else
                    Init(cam.transform.rotation);
            }

			if (Input.GetKeyDown (KeyCode.Tab)) {
				_controls.lookMoveSync = !_controls.lookMoveSync;
			}

#if CROSS_PLATFORM_INPUT
            _moveDir = new Vector3(CrossPlatformInputManager.GetAxis("Horizontal"), 0f, CrossPlatformInputManager.GetAxis("Vertical"));
#else
            _moveDir = new Vector3(Input.GetAxis("Horizontal"), 0f, Input.GetAxis("Vertical"));
#endif
            
            _moveSpeed = new Vector2(_moveDir.x, _moveDir.z).magnitude;
			if (_moveSpeed > 1f) {
				_moveSpeed = 1f;
				_moveDir.Normalize ();
			}
#if CROSS_PLATFORM_INPUT
            float dY = CrossPlatformInputManager.GetAxis("Mouse Y");
#else
            float dY = Input.GetAxis("Mouse Y");
#endif

            if (Mathf.Abs(Y + dY) > 85.0f)
                return;

#if CROSS_PLATFORM_INPUT
            X += CrossPlatformInputManager.GetAxis("Mouse X");
            Y += dY;
            Vector3 rotX = new Vector3(0f, CrossPlatformInputManager.GetAxis("Mouse X"), 0f);
            Vector3 rotY = new Vector3(-CrossPlatformInputManager.GetAxis("Mouse Y"), 0f, 0f);
#else
            X += Input.GetAxis("Mouse X");
            Y += dY;
            Vector3 rotX = new Vector3(0f, Input.GetAxis("Mouse X"), 0f);
            Vector3 rotY = new Vector3(-Input.GetAxis("Mouse Y"), 0f, 0f);
#endif

            _lookRot = Quaternion.Euler(rotX) * _lookRot; //absolute rotation

            _lookRot = _lookRot * Quaternion.Euler(rotY); //relative rotation

            _lookDir = _lookRot * Vector3.forward;

            _controls.lookRot = _lookRot;
            _controls.moveDir = _moveDir;

#if CROSS_PLATFORM_INPUT
			_controls.sprint = CrossPlatformInputManager.GetButton("Sprint");
            _controls.jump = CrossPlatformInputManager.GetButton("Jump");
            _controls.crouch = CrossPlatformInputManager.GetButton("Crouch");
            _controls.use = CrossPlatformInputManager.GetButton("Use");
            _controls.use = CrossPlatformInputManager.GetButton("Reload");
            _controls.use = CrossPlatformInputManager.GetButton("Pause");
            _controls.fire1 = CrossPlatformInputManager.GetButton("Fire1");
            _controls.fire2 = CrossPlatformInputManager.GetButton("Fire2");
            _controls.fire3 = CrossPlatformInputManager.GetButton("Fire3");
            _controls.pause = CrossPlatformInputManager.GetButton("Pause");
#else
            _controls.sprint = Input.GetButton("Sprint");
            _controls.jump = Input.GetButton("Jump");
            _controls.crouch = Input.GetButton("Crouch");
            _controls.use = Input.GetButton("Use");
            _controls.use = Input.GetButton("Reload");
            _controls.use = Input.GetButton("Pause");
            _controls.fire1 = Input.GetButton("Fire1");
            _controls.fire2 = Input.GetButton("Fire2");
            _controls.fire3 = Input.GetButton("Fire3");
            _controls.pause = Input.GetButton("Pause");
#endif
        }

        [ReadOnly]
        public float X;
        [ReadOnly]
        public float Y;
    }
}