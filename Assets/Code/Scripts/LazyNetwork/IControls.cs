﻿using UnityEngine;
using System.Collections;
using System;

namespace LazyNetwork{

	[Serializable]
	public struct ControllerState
	{
		public ControllerState(Vector3 moveDir, Quaternion lookRot, bool sprint, bool crouch, bool jump, bool use, bool reload, bool fire1, bool fire2, bool fire3, bool pause, bool lookMoveSync, ushort actionID)
		{
			this.moveDir = moveDir;
			this.lookRot = lookRot;
			this.sprint = sprint;
			this.crouch = crouch;
			this.jump = jump;
			this.use = use;
			this.reload = reload;
			this.fire1 = fire1;
			this.fire2 = fire2;
			this.fire3 = fire3;
			this.pause = pause;
			this.lookMoveSync = lookMoveSync;
			this.actionID = actionID;
		}

		public Vector3 moveDir;
		public Quaternion lookRot;
		public bool sprint, crouch, jump, use, reload, fire1, fire2, fire3, pause, lookMoveSync;
		public ushort actionID;
	}


	public interface IControls {

		ControllerState controls {
			get;
		}
	}
}
