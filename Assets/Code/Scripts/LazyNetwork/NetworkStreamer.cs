﻿using System;
using UnityEngine;
using UnityEngine.Networking;
using System.Collections;

/// <summary>
/// Network streamer is an object that invokes state data streaming depending on authority state of an object. 
/// invokes HostPlayerUpdate if isLocalPlayer == TRUE && hasAuthority == TRUE (player hosts server)
/// invokes LocalPlayerUpdate if isLocalPlayer == TRUE && hasAuthority == FALSE (player send data to the server)
/// invokes ServerUpdate if isServer == TRUE
/// </summary>
public abstract class NetworkStreamer : NetworkBehaviour
{

    // Use this for initialization  
    virtual protected void Start()
    {
		//Debug.Log ("isLocalPlayer? "+isLocalPlayer);
		//Debug.Log ("hasAuthority? "+hasAuthority);
		//Debug.Log ("isClient? "+isClient);

        Init();
        StartNetworkStreaming();
    }

    //Initialize default values and assign default pawn camera
    abstract protected void Init();

	//Sends state to the server
    abstract protected void SendState();

	//Updates state locally
    abstract protected void LocalStateUpdate();

	//invokes repetitive streaming methods
    virtual protected void StartNetworkStreaming()
    {
        //if controlling this pawn
        if (isLocalPlayer)
        {
            //if should update input local
            if (hasAuthority)
            {
                Invoke("HostPlayerUpdate", 0f);
            }
            //if should send input to owner (server)
            else
            {
                Invoke("LocalPlayerUpdate", 0f);
            }
        }
        if (isServer)
        {
            Invoke("ServerUpdate", 0f);
        }
    }

	//invokes while isServer == TRUE
    virtual protected void ServerUpdate()
    {
        if (isServer)
        {
            Invoke("ServerUpdate", GetNetworkSendInterval());
        }
    }

	//invokes while isLocalPlayer == TRUE && hasAuthority == FALSE
    virtual protected void LocalPlayerUpdate()
    {
        if (isLocalPlayer)
        {
            Invoke(hasAuthority ? "HostPlayerUpdate" : "LocalPlayerUpdate", GetNetworkSendInterval());
            SendState();
        }
    }

	//invokes isLocalPlayer == TRUE && hasAuthority == TRUE
    virtual protected void HostPlayerUpdate()
    {
		if (isLocalPlayer )
        {
            Invoke(hasAuthority ? "HostPlayerUpdate" : "LocalPlayerUpdate", GetNetworkSendInterval());
            LocalStateUpdate();
        }
    }
}
