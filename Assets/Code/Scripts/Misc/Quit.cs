﻿using UnityEngine;
using System.Collections;

public class Quit : MonoBehaviour {


	void Start () {
		//Cursor.lockState = CursorLockMode.Locked;
	}

	// Update is called once per frame
	void Update () {
		if (Screen.fullScreen) {
			Cursor.lockState = CursorLockMode.Locked;
		} else {	
			Cursor.lockState = CursorLockMode.None;
		}

		if (Input.GetMouseButton (0)) {
			Cursor.visible = true;
		}

		if (Input.GetKeyDown (KeyCode.Escape)) {
			//#if UNITY_WEBGL
			/*if ( !Cursor.visible ) {
				Cursor.lockState = CursorLockMode.None;
				Cursor.visible = true;
				Debug.Log ("unlocked "+Cursor.visible);

			} else {
				Cursor.lockState = CursorLockMode.Locked;
				Cursor.visible = false;
				Debug.Log ("locked "+Cursor.visible);
			}*/
			//#else
			Application.Quit();
			//#endif
		}
	}
}
