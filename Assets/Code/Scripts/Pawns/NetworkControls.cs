﻿using System;
using UnityEngine;
using UnityEngine.Networking;
using System.Collections;
using System.Collections.Generic;
using LazyNetwork;

/// <summary>
/// Network controls is an object that replicates player controller state
/// </summary>
[NetworkSettings(channel = 1, sendInterval = 0.1f)]
public class NetworkControls : NetworkStreamer, IControls
{
	//look rotation reference used for move direction calculations
    public Transform rotationReference;

	//desired move direction
    [SyncVar]
    public Vector3 moveDir;

	//desired look rotation
    [SyncVar]
    public Quaternion lookRot;

	//action triggers
    [SyncVar]
    public bool sprint, crouch, jump, use, reload, fire1, fire2, fire3, pause, lookMoveSync;

	//TODO: not implemented yet
    public delegate void UseAbilityDelegate(ushort abilityID);

	//controller state structure holds the most actual state of controller
	public ControllerState controls
	{
		get { return new ControllerState (moveDir, lookRot, sprint, crouch, jump, use, reload, fire1, fire2, fire3, pause, lookMoveSync, 0);  }
	}

    //[SyncEvent] 
    //public event UseAbilityDelegate EventUseAbility;

    //Initialize default values and assign default pawn camera
    override protected void Init()
    {
        Camera cam = GetComponentInChildren<Camera>();
        if (cam != null && rotationReference == null)
            rotationReference = cam.transform;

        if (cam != null)
        {
            //if local player - set camera as main
            if (isLocalPlayer)
            {
                cam.gameObject.SetActive(true);
                cam.tag = "MainCamera";
                //cam.transform.parent = null;
            }
            else
                cam.gameObject.SetActive(false);
        }
    }

    virtual protected Vector3 GetMoveDir()
    {
        return rotationReference.rotation * LocalInputManager.currentControls.moveDir;
    }

    virtual protected Quaternion GetLookRot()
    {
        return rotationReference.rotation;
    }

    //called if isLocalPlayer == true and has authority (host player)
    override protected void LocalStateUpdate()
    {
        moveDir = GetMoveDir();
        lookRot = GetLookRot();

        sprint = LocalInputManager.currentControls.sprint;
        crouch = LocalInputManager.currentControls.crouch;
        jump = LocalInputManager.currentControls.jump;
        use = LocalInputManager.currentControls.use;
        reload = LocalInputManager.currentControls.reload;
        fire1 = LocalInputManager.currentControls.fire1;
        fire2 = LocalInputManager.currentControls.fire2;
        fire3 = LocalInputManager.currentControls.fire3;
        pause = LocalInputManager.currentControls.pause;
		lookMoveSync = LocalInputManager.currentControls.lookMoveSync;
    }

    //called if isLocalPlayer without authority
    override protected void SendState()
    {
        Vector3 newMoveDir = GetMoveDir();
        if (newMoveDir != moveDir)
        {
            Cmd_ServerUpdateMoveDir(newMoveDir);
        }

        Quaternion newLookRot = GetLookRot();
        if (newLookRot != lookRot)
        {
            Cmd_ServerUpdateLookRot(newLookRot);
        }

        if (sprint != LocalInputManager.currentControls.sprint)
        {
            Cmd_Sprint(LocalInputManager.currentControls.sprint);
        }
        if (crouch != LocalInputManager.currentControls.crouch)
        {
            Cmd_Crouch(LocalInputManager.currentControls.crouch);
        }
        if (jump != LocalInputManager.currentControls.jump)
        {
            Cmd_Jump(LocalInputManager.currentControls.jump);
        }
        if (use != LocalInputManager.currentControls.use)
        {
            Cmd_Use(LocalInputManager.currentControls.use);
        }
        if (reload != LocalInputManager.currentControls.reload)
        {
            Cmd_Reload(LocalInputManager.currentControls.reload);
        }
        if (fire1 != LocalInputManager.currentControls.fire1)
        {
            Cmd_Fire1(LocalInputManager.currentControls.fire1);
        }
        if (fire2 != LocalInputManager.currentControls.fire2)
        {
            Cmd_Fire2(LocalInputManager.currentControls.fire2);
        }
        if (fire3 != LocalInputManager.currentControls.fire3)
        {
            Cmd_Fire3(LocalInputManager.currentControls.fire3);
        }
        if (pause != LocalInputManager.currentControls.pause)
        {
            Cmd_Pause(LocalInputManager.currentControls.pause);
        }
		if (lookMoveSync != LocalInputManager.currentControls.lookMoveSync) {
			Cmd_lookMoveSync(LocalInputManager.currentControls.lookMoveSync);
		}
    }

    
    [Command(channel = 1)] 
    virtual protected void Cmd_ServerUpdateMoveDir(Vector3 moveDir)
    {
        this.moveDir = moveDir;
    }
    [Command(channel = 1)] 
    virtual protected void Cmd_ServerUpdateLookRot(Quaternion lookRot)
    {
        this.lookRot = lookRot;
    }

    [Command(channel = 0)] 
    virtual protected void Cmd_Sprint(bool sprint)
    {
        this.sprint = sprint;
    }
    [Command(channel = 0)] 
    virtual protected void Cmd_Crouch(bool crouch)
    {
        this.crouch = crouch;
    }
    [Command(channel = 0)] 
    virtual protected void Cmd_Jump(bool jump)
    {
        this.jump = jump;
    }
    [Command(channel = 0)] 
    virtual protected void Cmd_Use(bool use)
    {
        this.use = use;
    }
    [Command(channel = 0)]  
    virtual protected void Cmd_Reload(bool reload)
    {
        this.reload = reload;
    } 
    [Command(channel = 0)] 
    virtual protected void Cmd_Fire1(bool fire1)
    {
        this.fire1 = fire1;
    }
    [Command(channel = 0)]
    virtual protected void Cmd_Fire2(bool fire2)
    {
        this.fire2 = fire2;
    }
    [Command(channel = 0)]
    virtual protected void Cmd_Fire3(bool fire3)
    {
        this.fire3 = fire3;
    }
    [Command(channel = 0)]
    virtual protected void Cmd_Pause(bool pause)
    {
        this.pause = pause;
    }
	[Command(channel = 0)]
	virtual protected void Cmd_lookMoveSync(bool lookMoveSync)
	{
		this.lookMoveSync = lookMoveSync;
	}
    
}