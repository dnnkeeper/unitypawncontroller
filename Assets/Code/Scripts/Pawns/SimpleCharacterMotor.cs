﻿using UnityEngine;
using System.Collections;

public class SimpleCharacterMotor : MonoBehaviour {

    public NetworkControls controls;
    public CharacterController characterController;

    public float speed = 0.2f;

    public float m_JumpSpeed = 0.1f;

    Vector3 m_MoveDir;
    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void FixedUpdate()
    {
        if (characterController.isGrounded)
        {
            m_MoveDir.x = controls.moveDir.x * speed;
            m_MoveDir.y = 0f;//-1.0f * Time.fixedDeltaTime;
            m_MoveDir.z = controls.moveDir.z * speed;

            if (controls.jump)
            {
                m_MoveDir.y = m_JumpSpeed;
                //PlayJumpSound();
                //m_Jump = false;
                //m_Jumping = true;
            }
        }
        else
        {
            m_MoveDir += Physics.gravity / 60.0f * Time.fixedDeltaTime;
            Debug.DrawRay(transform.position, m_MoveDir, Color.red);
        }
        characterController.Move(m_MoveDir);

    }
}
