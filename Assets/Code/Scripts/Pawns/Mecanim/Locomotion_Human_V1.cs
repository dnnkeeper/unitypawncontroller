﻿//#define DRAW_RAYS
#define DRAW_GRAB_RAYS

using UnityEngine;
using System.Collections;
using UnityStandardAssets.Utility;
using UnityEngine.Networking;
using LazyNetwork;
using Random = UnityEngine.Random;

public class Locomotion_Human_V1 : MonoBehaviour {

	#region Initial Settings

	[Header("Initial Settings")]

	public GameObject controllerGameObject;

	protected IControls controller;

	public Animator animator;

	public CharacterController characterController;

	public Rigidbody rb;

	public AudioSource audioSource;

	#endregion

	#region Options

	[Header("Options")]

    public bool LookAndMoveSync;

	public bool FixedUpdateMove = true;

	public float runSpeed = 5.67f;

	public float walkSpeed = 1.55f;

	#endregion

	#region Settings

	[SerializeField] protected AudioClip[] footstepSounds;    // an array of footstep sounds that will be randomly selected from.

	[SerializeField] protected float _SpeedAccelDampTime = 0.25f;
	[SerializeField] protected float _SpeedDecelDampTime = 0.2f;
	[SerializeField] protected float _AnguarSpeedDampTime = 0.33f; //0.25f
	[SerializeField] protected float _DirectionResponseTime = 0.33f; //0.2f

	#endregion

	#region State Context

    protected bool applyRootMotion;

	protected Vector3 animatorVelocity;

    protected Vector3 realVelocity, lastPos;

	protected Vector3 characterForward,preturnForward;

	protected int accelCount;

    protected float desiredAcceleration, oldMovDirY;

    public bool isAiming = false;

	public bool isFiring = false;

	public bool isReloading = false;

	protected float lastFootStep;

	protected float _stateTime;

	//IK
	[Header("IK Settings")]
	protected Vector3 lookAtDir;
	protected Vector3 lookDir;
	protected float lookAtWeight;
	public float lookAtSpeed = 1.0f;
	public float lookAtMaxAngle = 90.0f;
	public Vector3 lookAtWeights = new Vector3(0.2f, 0.6f, 1.0f);
	public float AimAngleX = 12.0f;
	public float AimAngleY = 12.0f;
	public float charHeight = 1.65f;
	public float stepHeight = 0.12f;
	protected Vector3 leftFootPos, rightFootPos;
	protected bool leftStep, rightStep;
	protected Vector3 motorVelocity;
	protected float desiredSpeed;
	protected Vector3 groundNormal;
	protected float heightAboveGround = 0;

	#endregion
 
	#region Hashes
    protected int _ActionsId = 0;
    protected int _DesiredSpeedId = 0;
    protected int _RealSpeedId = 0;
    protected int _LookDirId = 0;
    protected int _AngularSpeedId = 0;
    protected int _MoveDirXId = 0;
    protected int _MoveDirYId = 0;
    protected int _AccelerationId = 0;
    protected int _SlopeId = 0;
    protected int _HeightId = 0;
    protected int _GrabId = 0;
    protected int _JumpId = 0;
    protected int _IsGroundedId = 0;
    protected int _IsRunningId = 0;
    protected int _RealSpeedXId = 0;
    protected int _RealSpeedYId = 0;
    protected int _GrabHeightId = 0;
	protected int _PlantNTurnId = 0;
	protected int _StateTimeId = 0;
	#endregion

    public Vector3 collisionCenter = new Vector3(0, 1, 0);
	public Vector3 defaultCollisionCenter = new Vector3(0, 1, 0);
	protected float defaultCollisionRadius = 0.2f;
    public float characterHeight;
    public LayerMask collisionMask;

    public bool isGrounded, isLanding, inTransition, inIdle, inTurn, inWalkRun, inClimb, inAir, inRoll, inGetUp, inWalkUp;

    public Vector3 _GrabPos = Vector3.zero;
    public Quaternion _GrabRot = Quaternion.identity;

	private float lastMecanimUpdateTime;

	private ControllerState controls;
	private bool bGrab, PlantNTurn, isRunning;
	private float dir, oldDir, grabHeight;
	private Vector3 lookDirOnGround;
	private Vector2 moveDir2D;

	Vector3 gravity;


	void Reset(){
		if (controllerGameObject == null){
			if (transform.parent != null) {
				controllerGameObject = transform.parent.gameObject;
			} else {
				controllerGameObject = transform.gameObject;
			}
		}
		if (controllerGameObject != null) {
			controller = controllerGameObject.GetComponent<IControls>(); 
		}

		characterController = controllerGameObject.GetComponent<CharacterController> ();
		animator = GetComponent<Animator>();
		rb = characterController.GetComponent<Rigidbody> ();
		collisionMask = LayerMask.GetMask ("Default");
	}

    void Awake()
	{
		if (controllerGameObject != null && controller == null)
			controller = controllerGameObject.GetComponent<IControls>(); //(IControls)src;
		if (controllerGameObject == null || controller == null) {
			Debug.LogWarning ("no network controller, use local");
			//enabled = false;
			controller = LocalInputManager.Instance;
		}

        InitializeLocomotion();
        lastPos = transform.position;
        if (animator == null)
			animator = GetComponent<Animator>();
		if (rb == null)
			rb = characterController.GetComponent<Rigidbody> ();

		InvokeRepeating ("UpdateMecanim", 0f, 1f / 15f);
	}

    virtual protected void InitializeLocomotion()
    {
        //collisionMask = 1 << LayerMask.GetMask("default");

        _LookDirId = Animator.StringToHash("LookDir");
        _MoveDirXId = Animator.StringToHash("MoveDirX");
		_MoveDirYId = Animator.StringToHash("MoveDirY");

        _RealSpeedXId = Animator.StringToHash("RealSpeedX");
        _RealSpeedYId = Animator.StringToHash("RealSpeedY");

        _DesiredSpeedId = Animator.StringToHash("DesiredSpeed");
        _RealSpeedId = Animator.StringToHash("RealSpeed");

        _AngularSpeedId = Animator.StringToHash("AngularSpeed");

        _IsGroundedId = Animator.StringToHash("isGrounded");
        _IsRunningId = Animator.StringToHash("isRunning");
        _HeightId = Animator.StringToHash("Height");
        
        _ActionsId = Animator.StringToHash("ActionID");

        _AccelerationId = Animator.StringToHash("Acceleration");
        _SlopeId = Animator.StringToHash("Slope");

		_GrabId = Animator.StringToHash("Grab");
		_JumpId = Animator.StringToHash("Jump");

        _GrabHeightId = Animator.StringToHash("GrabHeight");
		_PlantNTurnId = Animator.StringToHash("PlantNTurn");

		_StateTimeId = Animator.StringToHash("StateTime");

		characterForward = transform.forward;
		defaultCollisionCenter = transform.InverseTransformPoint(characterController.bounds.center);
		collisionCenter = defaultCollisionCenter;//transform.InverseTransformPoint(characterController.bounds.center);
        characterHeight = characterController.height;
		defaultCollisionRadius = characterController.radius;

        //DisableRagdoll();
        //EnableRagdoll();
    }

    virtual protected void GetMecanimState()
    {
        inTransition = animator.IsInTransition(0);// && animator.GetAnimatorTransitionInfo(0).normalizedTime < 0.5f;

		isLanding = inTurn = inWalkRun = false;

        AnimatorStateInfo stateInfo = animator.GetCurrentAnimatorStateInfo(0);

        inWalkRun = stateInfo.IsTag("WalkRun");

        if (!inWalkRun)
        {
			AnimatorStateInfo nextState = animator.GetNextAnimatorStateInfo (0);
			inTurn = stateInfo.IsTag ("Turn") || nextState.IsTag ("Turn");// && !nextState.IsTag ("WalkRun");
			if (!inTurn) {
				isLanding = stateInfo.IsTag ("Landing") || nextState.IsTag ("Landing");
				if (!isLanding)
					inClimb = stateInfo.IsTag("Climb");
			}
        }
    }
    
    public void OnAnimatorMove()
	{
		if (!applyRootMotion)
			return;

		Quaternion dRot = animator.deltaRotation;
		
		transform.rotation *= dRot;

		Vector3 dp = animator.deltaPosition;
        
		animatorVelocity = dp;
	}

	void LateUpdate()
	{
        if (animator == null || animator.GetBoneTransform(HumanBodyBones.LeftFoot) == null)
			return;

		CheckFootstep ();

		lastPos = transform.position;

        //Debug.DrawLine(transform.position, leftFoot, Color.blue);
        //Debug.DrawLine(transform.position, rightFoot, Color.yellow);
    }


	void OnAnimatorIK(int Layer) {

       //if (!initialized)
        //return;

        bool aiming = isAiming;
		
		Vector3 headPos = transform.position+transform.up*charHeight;

		if (Layer == 0 && !aiming){
			
			//FootPlacementX(animator,FootPlacement,TraceDist,UpDist);//,UpDist, animator.GetFloat("Speed")/10,animator.GetBoneTransform(HumanBodyBones.LeftToes),animator.GetBoneTransform(HumanBodyBones.RightToes),animator.GetBoneTransform(HumanBodyBones.LeftFoot), animator.GetBoneTransform(HumanBodyBones.RightFoot));
			
			Vector3 camdir = controller.controls.lookRot * Vector3.forward;
			
			//lookAtWeight = Mathf.Lerp(lookAtWeight, 0.0f, Mathf.Min(1.0f, 0.01f*Vector3.Angle(camdir,lookAtDir) ) );
			lookAtDir = Vector3.Lerp(lookAtDir, camdir,1.5f*Time.smoothDeltaTime);
			
			//Debug.DrawRay(headPos,transform.forward, Color.red);
			//Debug.DrawRay(headPos,lookDirection*100.0f, Color.magenta);
			if (Vector3.Angle(camdir,characterForward) < lookAtMaxAngle) // Vector3.Cross(camdir,characterForward).y > 0)//
			{
				animator.SetLookAtPosition(headPos+lookAtDir);
				lookAtWeight = Mathf.Lerp(lookAtWeight, 1.0f, lookAtSpeed*Time.smoothDeltaTime );

				#if DRAW_RAYS
				Debug.DrawRay(headPos,camdir, Color.green);
				#endif
			}
			else{
				lookAtWeight = Mathf.Lerp(lookAtWeight, 0.0f, 2.0f*Time.smoothDeltaTime );
				#if DRAW_RAYS
				Debug.DrawRay(headPos,camdir, Color.gray);
				#endif
			}
			animator.SetLookAtWeight(lookAtWeight, lookAtWeights.x, lookAtWeights.y, lookAtWeights.z );
		}
		else if (Layer == 2 && aiming){
			
			Vector3 camdir = Quaternion.AngleAxis(AimAngleY, transform.right) * Quaternion.AngleAxis(AimAngleX, transform.up)*lookDir;
			//Debug.DrawRay(headPos,transform.right, Color.green);
			//Debug.DrawRay(headPos,transform.up, Color.yellow);

			//animator.GetBoneTransform(HumanBodyBones.Head).position;
			
			lookAtWeight = 1.0f;
			lookAtDir = Vector3.Lerp(lookAtDir, camdir,3.0f*Time.smoothDeltaTime);
			
			if (Vector3.Angle(camdir,characterForward) < 120.0f) // Vector3.Cross(camdir,characterForward).y > 0)//
			{
				animator.SetLookAtPosition(headPos+lookAtDir);
				lookAtWeight = 1.0f;
				#if DRAW_RAYS
				Debug.DrawRay(headPos,camdir*10.0f, Color.green);
				Debug.DrawRay(headPos,lookDir*10.0f, Color.green);
				#endif
			}
			else{
				lookAtWeight = Mathf.Lerp(lookAtWeight, 0.0f, Time.smoothDeltaTime );
				//Debug.DrawRay(headPos,camdir, Color.gray);
			}
			animator.SetLookAtWeight(lookAtWeight, 1.0f, 1.0f, 1.0f );
		}
	}

    virtual protected bool RaycastGround(out float height, out Vector3 normal, float radius = 0.3f)
    {
        bool bGrounded = false;
        height = 10.0f;
        normal = Vector3.zero;
        RaycastHit rhit;
        Vector3 rndS = Random.onUnitSphere;
        rndS.y = 0;
        Ray r = new Ray(transform.position + rndS * 0.2f + transform.up, -transform.up);
        //if (Physics.SphereCast(r, radius, out rhit, 10.0f, collisionMask))
		if (Physics.Raycast(r, out rhit, characterHeight*3.0f, collisionMask))
        {
            //Debug.DrawLine(r.origin, rhit.point, Color.red, Time.deltaTime);

            height = rhit.distance;
            normal = rhit.normal;

			if (height <= characterHeight)
            {
				bGrounded = true;
            }
            else
            {
				bGrounded = false;
            }
        }
		return bGrounded;
    }

    void SetRealSpeed(Vector3 v, float f, float deltaTime)
    {
		animator.SetFloat(_RealSpeedXId, v.x, f, deltaTime);
		animator.SetFloat(_RealSpeedYId, v.z, f, deltaTime);
		animator.SetFloat(_RealSpeedId, v.magnitude, f, deltaTime);
    }
		
	/// <summary>
	/// 			MoveDir / LookDir
	/// </summary>
	void ControlLookMoveAnim(ref ControllerState controls, out Vector3 vLookDir, out Vector3 vLookDirOnGround, out Vector2 vMoveDir2D){

		Vector3 desiredVelocityOnGround = Vector3.ProjectOnPlane(controls.moveDir, groundNormal);

		vLookDir = (controls.lookMoveSync) ? desiredVelocityOnGround : Vector3.ProjectOnPlane( controls.lookRot * Vector3.forward, groundNormal );

		if (vLookDir.sqrMagnitude == 0)
			vLookDirOnGround = transform.forward;
		else
			vLookDirOnGround = vLookDir;

		//aim 45 degrees rotation
		vLookDirOnGround = (isAiming) ? Quaternion.AngleAxis(45.0f, groundNormal) * vLookDirOnGround : vLookDirOnGround;

		if (desiredVelocityOnGround.sqrMagnitude > 0) {
			float a = vLookDirOnGround.AngleSignedWith( desiredVelocityOnGround, groundNormal);
			vMoveDir2D = new Vector2 (Mathf.Sin (a), Mathf.Cos (a)).normalized;
		} else
			vMoveDir2D = Vector2.zero;
		
	}

	/// <summary>
	/// Updates jump and grab.
	/// </summary>
	void ControlJumpNClimb(ref ControllerState controls, ref Vector3 lookDirOnGround, out bool bGrab, float deltaTime){

		bool wantToJump = controls.jump;
		bool blocked = false;
		bGrab = false;

		Vector3 blockTraceStart = transform.position + transform.up*(characterHeight*0.3f) - lookDirOnGround*characterController.radius;
		Vector3 blockTraceDir = lookDirOnGround;
		float traceLength = characterHeight;
		foreach (RaycastHit block_hit in Physics.RaycastAll(blockTraceStart, blockTraceDir, traceLength+characterController.radius))
		{
			if ( block_hit.collider.tag != "Player")
			{
				#if DRAW_RAYS
				Debug.DrawLine(blockTraceStart, block_hit.point, Color.yellow);
				#endif
				if ( Vector3.Dot( -block_hit.normal, blockTraceDir) > 0.5f ) {
					blocked = true;
					blockTraceDir = -block_hit.normal;
				}
				break;
			}
		}

		grabHeight = characterHeight*2f; //inAir ? characterHeight/2f : characterHeight;

		if (!blocked) {
			blockTraceStart += transform.up * characterHeight;
			grabHeight -= characterHeight;
		} else {
			traceLength *= 0.3f;
		}

		#if DRAW_GRAB_RAYS
		Debug.DrawRay(blockTraceStart, blockTraceDir*traceLength, Color.black);
		#endif

		foreach (RaycastHit block_hit in Physics.RaycastAll(blockTraceStart, blockTraceDir, traceLength+characterController.radius))
		{
			if ( block_hit.collider.tag != "Player")
			{
				#if DRAW_GRAB_RAYS
				Debug.DrawLine(blockTraceStart, block_hit.point, Color.yellow);
				#endif

				Vector3 v = new Vector3 (-block_hit.normal.x, 0, -block_hit.normal.z);
				_GrabRot = v != Vector3.zero ? Quaternion.LookRotation (v, Vector3.up) : transform.rotation;


				float grabLength = inAir ? grabHeight + characterHeight : grabHeight;

				Vector3 traceStart = block_hit.point + blockTraceDir * 0.02f + Vector3.Cross(transform.up,blockTraceDir) * 0.3f + transform.up * grabHeight;
				RaycastHit grab_hit;
				if (Physics.Raycast(traceStart, -transform.up, out grab_hit, grabLength))
				{
					#if DRAW_GRAB_RAYS
					Debug.DrawLine(grab_hit.point + Vector3.up, grab_hit.point, Color.green);
					#endif

					if (wantToJump)
					{
						_GrabPos = grab_hit.point + Vector3.up*0.06f;

						bGrab = true;

						//animator.SetFloat(_GrabHeightId, grab_hit.point.y - transform.position.y);
						//animator.SetBool(_JumpId, false);
						grabHeight = grab_hit.point.y - transform.position.y;
						wantToJump = false;

						break;
					}
				}
				#if DRAW_GRAB_RAYS
				else
					Debug.DrawLine(traceStart, traceStart - transform.up * grabLength, Color.red);
				#endif
				break;
			}
		}

	}

	void MoveCharacter(float deltaTime){

		//Send message to notify other components about landing (eg. to enable footplacement)
		if (characterController.isGrounded != isGrounded) 
		{
			isGrounded = characterController.isGrounded;
			gameObject.SendMessage ("OnGrounded", isGrounded, SendMessageOptions.DontRequireReceiver);
		}

		if (isGrounded || inClimb) 
		{
			if (inClimb)
				gravity = Vector3.zero;
			else
				gravity = -5.0f * transform.up;
			
			motorVelocity = (animatorVelocity / deltaTime)/Time.timeScale;
		} 

		gravity = gravity + Physics.gravity * deltaTime;

		characterController.Move ( (motorVelocity+gravity) * deltaTime);

		//motorVelocity = motorVelocity + Physics.gravity * deltaTime;
		//characterController.Move (motorVelocity * deltaTime);
		#if DRAW_RAYS
		Debug.DrawRay(transform.position, motorVelocity, Color.red);
		#endif

		// Vt + at^2/2
		//characterController.Move ( motorVelocity * deltaTime + Physics.gravity * deltaTime * deltaTime / 2.0f );
		//motorVelocity = motorVelocity + Physics.gravity * deltaTime;
	}

	void UpdateMecanim(){
		
		float deltaTime = Time.time - lastMecanimUpdateTime;

		lastMecanimUpdateTime = Time.time;

		ControlLookMoveAnim (ref controls, out lookDir, out lookDirOnGround, out moveDir2D);

		groundNormal = transform.up;
		RaycastGround (out heightAboveGround, out groundNormal, characterController.radius);
		characterForward = transform.forward;

		Vector3 collisionCenter = Vector3.up;
		float collisionHeight = characterHeight;

		applyRootMotion = true;
		bGrab = false;

		if (inClimb) 
		{
			//Rotate character to match grab rotation
			transform.rotation = Quaternion.Lerp (transform.rotation, _GrabRot, 3.0f * deltaTime);

			if (inTransition) 
			{
				collisionCenter = defaultCollisionCenter;
			} 
			else 
			{
				bGrab = true;

				if (_stateTime > animator.GetFloat ("MatchStart"))
				{
					collisionCenter = characterController.transform.InverseTransformDirection (transform.up * 1.2f - characterForward * 0.25f);
					collisionHeight = 1.2f;
				}

				Vector3 wmask = new Vector3 (1, 1, 1);
				animator.MatchTarget (_GrabPos, Quaternion.identity, AvatarTarget.RightHand, new MatchTargetWeightMask (wmask, 0), animator.GetFloat ("MatchStart"), animator.GetFloat ("MatchEnd"));//0.181f, 0.297f);
			}
		}
		else 
		{
			if (!isGrounded)
			{
				applyRootMotion = false;
			}

			if (inRoll)
			{
				collisionCenter = Vector3.up * 0.6f;
				collisionHeight = 1.2f;
			}
			else
			{
				collisionCenter = defaultCollisionCenter;
				collisionHeight = characterHeight;
			}

			ControlJumpNClimb (ref controls, ref lookDirOnGround, out bGrab, deltaTime);
		}

		characterController.center = Vector3.Lerp( characterController.center, collisionCenter, 20.0f * deltaTime );
		characterController.height = Mathf.Lerp( characterController.height, collisionHeight, 20.0f * deltaTime );

		isRunning = controller.controls.sprint;

		desiredSpeed = controller.controls.moveDir.magnitude * (isRunning ? runSpeed : walkSpeed);

		desiredAcceleration = ((moveDir2D*desiredSpeed).y - realVelocity.z);


		Debug.DrawRay (transform.position+transform.up, lookDirOnGround, Color.green );

		oldDir = animator.GetFloat(_LookDirId);
		dir = 0;
		if (lookDirOnGround.sqrMagnitude > 0)
		{
			dir = characterForward.AngleSignedWith( lookDirOnGround, groundNormal) * Mathf.Rad2Deg;
			if (Mathf.Abs(dir) > 170.0f)
			{
				if (Mathf.Abs(oldDir) <= 10.0f)
				{
					dir = Mathf.Abs(dir);
				}
				else
					dir = Mathf.Abs(dir) * Mathf.Sign(oldDir);
			}
		}

		if (Mathf.Abs(oldDir) > 90.0f)
			PlantNTurn = true;
		else
			PlantNTurn = false;

	}

	void FixedUpdate()
	{
		if (FixedUpdateMove)
			MoveCharacter (Time.fixedDeltaTime);
	}

	void Update()
    {
		float deltaTime = Time.deltaTime * Time.timeScale;

		//UpdateMecanim ();

		if (!FixedUpdateMove)
			MoveCharacter (Time.deltaTime);

		controls = controller.controls;

		realVelocity = transform.InverseTransformDirection(characterController.velocity); 

		GetMecanimState ();

		_stateTime = (animator.GetCurrentAnimatorStateInfo (0).normalizedTime*10f)%10f/10f;

		//Don't update LookDir while turning to prevent animation blending
		if ( inTurn )
		{
			Debug.DrawRay (transform.position+transform.up, preturnForward, Color.red );

			//rapidly changed mind in turn or increased angle
			if ( Mathf.Abs( dir - oldDir) > 120.0f || Mathf.Abs( dir ) > Mathf.Abs(oldDir) ) // 
				animator.SetFloat(_LookDirId, dir, 0.3f, deltaTime);

			// Attract speed only to walkSpeed or runSpeed when turning
			float currentSpeed = animator.GetFloat(_DesiredSpeedId );
			if (currentSpeed > walkSpeed + (runSpeed-walkSpeed)/2.0f)
				currentSpeed = runSpeed;
			else
				currentSpeed = walkSpeed;

			animator.SetFloat (_DesiredSpeedId, Mathf.Max (currentSpeed, desiredSpeed), _SpeedAccelDampTime, deltaTime);
		}
		else{
			preturnForward = characterForward;

			//preturnDir = dir;

			animator.SetFloat(_DesiredSpeedId, desiredSpeed, desiredAcceleration > 0f ? _SpeedAccelDampTime : _SpeedDecelDampTime, deltaTime);

			animator.SetFloat(_LookDirId, dir, 0.0f, deltaTime);    
		}		

		animator.SetFloat(_GrabHeightId, grabHeight);
		animator.SetBool(_JumpId, false);

		animator.SetFloat (_StateTimeId, _stateTime);

		animator.SetBool(_GrabId, bGrab);
		animator.SetBool(_JumpId,controller.controls.jump);
		animator.SetBool(_PlantNTurnId, PlantNTurn);
		animator.SetBool(_IsRunningId, isRunning && moveDir2D.sqrMagnitude > 0f);
		animator.SetBool(_IsGroundedId, isGrounded);

		animator.SetFloat(_AccelerationId, desiredAcceleration, 0.01f, deltaTime);
		animator.SetFloat(_MoveDirXId, moveDir2D.x, 0.15f, deltaTime);
		animator.SetFloat(_MoveDirYId, moveDir2D.y, 0.3f, deltaTime);

		//slow down angular speed change in walk/run state
		animator.SetFloat(_AngularSpeedId, Mathf.Clamp( dir/ (inTurn? _DirectionResponseTime*2f : _DirectionResponseTime), -180.0f, 180.0f), _AnguarSpeedDampTime, deltaTime);
		animator.SetFloat(_HeightId, heightAboveGround, 0.05f * heightAboveGround, deltaTime);

		SetRealSpeed(realVelocity, isLanding ? 1.0f : 0.1f, deltaTime);
	}

	private void CheckFootstep()
	{
		leftFootPos = transform.InverseTransformPoint( animator.GetBoneTransform(HumanBodyBones.LeftFoot).position );
		rightFootPos = transform.InverseTransformPoint( animator.GetBoneTransform(HumanBodyBones.RightFoot).position );

		if ( leftFootPos.y < stepHeight )
		{
			if (!leftStep){
				leftStep = true;
				Footstep();
			}
		}else
			leftStep = false;

		if ( rightFootPos.y < stepHeight )
		{
			if (!rightStep){
				rightStep = true;
				Footstep();
			}
		}
		else
			rightStep = false;
	}

	private void Footstep()
	{
		if (Time.time-lastFootStep<0.15f || !characterController.isGrounded || desiredSpeed < 0.1f )
		{
			return;
		}

		lastFootStep = Time.time;

        // pick & play a random footstep sound from the array,
        // excluding sound at index 0
        if (footstepSounds.Length > 0)
        {
            int n = Random.Range(1, footstepSounds.Length);
            audioSource.clip = footstepSounds[n];

            AudioSource.PlayClipAtPoint(footstepSounds[n], transform.position);

            //m_AudioSource.PlayOneShot(m_AudioSource.clip);
            // move picked sound to index 0 so it's not picked next time
            footstepSounds[n] = footstepSounds[0];
            footstepSounds[0] = audioSource.clip;
        }
	}
	
	void LandingEvent()
	{}
}
