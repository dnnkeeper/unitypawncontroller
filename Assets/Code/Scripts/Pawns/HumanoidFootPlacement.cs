﻿using UnityEngine;
using System.Collections;

public class HumanoidFootPlacement : MonoBehaviour {

	//public bool active = true;

	public float customTranslation = 0f;
	public float bodyTranslationSpeed = 10.0f;
	public float footTranslationSpeed = 10.0f;

	public Animator animator;
	public float footRadius = 0.1f;
	public float maxDown = 0.4f;
	public float maxUp = 0.9f;

	protected Vector3 targetLocalPos = Vector3.zero;
	protected float leftFootTranslation;
	protected float rightFootTranslation;

	protected Transform parent;
	protected float translation;

	public LayerMask collisionMask;

	public float leftWeight = 0f;
	public float rightWeight = 0f;
	public Vector3 leftFootTarget, rightFootTarget;

	void Start()
	{
		if (animator == null)
			animator = GetComponent<Animator> ();

		parent = transform.parent;

		if (parent == null) {
			parent = transform;
		}

		leftFootTarget = animator.GetBoneTransform (HumanBodyBones.LeftFoot).position;
		rightFootTarget = animator.GetBoneTransform (HumanBodyBones.RightFoot).position;

		/*
		Vector3 leftFootTarget =  animator.GetBoneTransform (HumanBodyBones.LeftFoot ).position;
		Vector3 rightFootTarget = animator.GetBoneTransform (HumanBodyBones.RightFoot).position;
		Vector3 midPoint = leftFootTarget + ( rightFootTarget - leftFootTarget ) / 2.0f;

		defaultMidPointHeight = (parent.position - leftFootTarget + ( rightFootTarget - leftFootTarget ) / 2.0f).magnitude;
		//Debug.Log ("defaultMidPointHeight "+defaultMidPointHeight);
		*/
	}

	void OnAnimatorIK(int Layer){

		if (Layer == 0 && enabled)
			FootPlacement (animator, maxDown, maxUp);

	}

	void OnGrounded(bool isGrounded){
		enabled = isGrounded;
	}

	void FootPlacement(Animator animator, float maxDown, float maxUp) {

		float leftWeightNew = 0f;
		float rightWeightNew = 0f;

		float traceLength = maxDown + maxUp;

		Transform leftFootTransform = animator.GetBoneTransform (HumanBodyBones.LeftFoot);
		Transform rightFootTransform = animator.GetBoneTransform (HumanBodyBones.RightFoot);

		Vector3 leftFootPos = leftFootTransform.position;

		Vector3 rightFootPos = rightFootTransform.position;

		Ray leftFootRay  = new Ray( leftFootPos  + parent.up*maxUp, -parent.up );
		Ray rightFootRay = new Ray( rightFootPos + parent.up*maxUp, -parent.up );

		Vector3 leftHitLocal = Vector3.zero;
		Vector3 rightHitLocal = Vector3.zero;

		RaycastHit leftFootHit, rightFootHit;
		if (Physics.Raycast (leftFootRay, out leftFootHit, traceLength, collisionMask)) {
			//if (Physics.SphereCast (leftFootRay, footRadius,  out leftFootHit, traceLength, collisionMask)) {
			if (leftFootHit.collider.tag != "Player" && Vector3.Angle (leftFootHit.normal, transform.up) < 45.0f) {

				leftHitLocal = parent.InverseTransformPoint (leftFootHit.point);

				leftFootTranslation = Mathf.Lerp (leftFootTranslation, transform.InverseTransformPoint (leftFootHit.point).y, footTranslationSpeed * Time.deltaTime); //parent.up * (maxUp+traceLength - leftFootHit.distance);//leftFootHit.point - leftFootTransform.position;
				Debug.DrawLine (leftFootHit.point, leftFootTransform.position, leftFootHit.point.y > leftFootTransform.position.y ? Color.green : Color.red);

				//Debug.DrawLine (leftFootRay.origin, leftFootHit.point, Color.magenta);

				leftWeightNew = 1.0f;
			}
		} else {
			Debug.DrawRay (leftFootRay.origin, leftFootRay.direction * traceLength, Color.grey);
			leftHitLocal = parent.InverseTransformPoint ( leftFootRay.origin+leftFootRay.direction * traceLength);
		}

		if (Physics.Raycast (rightFootRay, out rightFootHit, traceLength, collisionMask)) {
			//if (Physics.SphereCast (rightFootRay, footRadius, out rightFootHit, traceLength, collisionMask)) {
			if (rightFootHit.collider.tag != "Player" && Vector3.Angle (rightFootHit.normal, transform.up) < 45.0f) {

				rightHitLocal = parent.InverseTransformPoint (rightFootHit.point);

				rightFootTranslation = Mathf.Lerp (rightFootTranslation, transform.InverseTransformPoint (rightFootHit.point).y, footTranslationSpeed * Time.deltaTime);//parent.up * (maxUp+traceLength - rightFootHit.distance);//rightFootHit.point - rightFootTransform.position;
				Debug.DrawLine (rightFootHit.point, rightFootTransform.position, rightFootHit.point.y > rightFootTransform.position.y ? Color.green : Color.red);
				//Debug.DrawLine (rightFootRay.origin, rightFootHit.point, Color.magenta);

				rightWeightNew = 1.0f;
			}
		} else {
			Debug.DrawRay (rightFootRay.origin, rightFootRay.direction * traceLength, Color.grey);
			rightHitLocal = parent.InverseTransformPoint ( rightFootRay.origin+rightFootRay.direction * traceLength );
		}
		

		leftFootPos = leftFootTransform.position + parent.up*(leftFootTranslation+customTranslation);

		rightFootPos = rightFootTransform.position + parent.up*(rightFootTranslation+customTranslation);


		Vector3 lowestFootPosLocal = (leftHitLocal.y < rightHitLocal.y) ? leftHitLocal : rightHitLocal;

		translation = Mathf.Max(-maxDown, Mathf.Min (0f, lowestFootPosLocal.y) );

		targetLocalPos = new Vector3 (targetLocalPos.x, Mathf.Lerp(targetLocalPos.y, translation, bodyTranslationSpeed*Time.deltaTime), targetLocalPos.z); 

		Vector3 targetPos = parent.TransformPoint (targetLocalPos);

		Debug.DrawLine (parent.position, targetPos, parent.position.y < targetPos.y ? Color.green :  Color.red);

		transform.localPosition = targetLocalPos;

		leftWeight = leftWeightNew;//Mathf.Lerp(leftWeight, leftWeightNew, translationSpeed*Time.deltaTime);
		rightWeight = rightWeightNew;//Mathf.Lerp(rightWeight, rightWeightNew, translationSpeed*Time.deltaTime);

		animator.SetIKPosition(AvatarIKGoal.LeftFoot, leftFootPos );
		animator.SetIKPositionWeight(AvatarIKGoal.LeftFoot, leftWeight);
		animator.SetIKRotation (AvatarIKGoal.LeftFoot, Quaternion.FromToRotation(transform.up, leftFootHit.normal) * leftFootTransform.rotation);
		animator.SetIKRotationWeight(AvatarIKGoal.LeftFoot, leftWeight);

		animator.SetIKPosition(AvatarIKGoal.RightFoot, rightFootPos );
		animator.SetIKPositionWeight(AvatarIKGoal.RightFoot, rightWeight);
		animator.SetIKRotation (AvatarIKGoal.RightFoot, Quaternion.FromToRotation(transform.up, rightFootHit.normal) * rightFootTransform.rotation);
		animator.SetIKRotationWeight(AvatarIKGoal.RightFoot, rightWeight);
	}
}
