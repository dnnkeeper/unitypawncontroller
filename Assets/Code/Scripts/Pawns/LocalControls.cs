﻿using UnityEngine;
using System.Collections;
using LazyNetwork;

public class LocalControls : MonoBehaviour, IControls {
	
	public Transform rotationReference;

	public float lazyTime = 0.1f;

	public Vector3 moveDir;

	public Quaternion lookRot;

	public bool sprint, crouch, jump, use, reload, fire1, fire2, fire3, pause, lookMoveSync;

	public delegate void UseAbilityDelegate(ushort abilityID);

	public ControllerState controls
	{
		get { return new ControllerState (moveDir, lookRot, sprint, crouch, jump, use, reload, fire1, fire2, fire3, pause, lookMoveSync, 0);  }
	}

	//[SyncEvent] 
	//public event UseAbilityDelegate EventUseAbility;

	//Initialize default values and assign default pawn camera
	protected void Init()
	{
		Camera cam = GetComponentInChildren<Camera>();
		if (cam != null && rotationReference == null)
			rotationReference = cam.transform;

		if (cam != null)
		{
			
			cam.gameObject.SetActive(true);
			cam.tag = "MainCamera";
			//cam.transform.parent = null;
		}
	}

	void Update(){
		LocalStateUpdate ();
	}

	virtual protected Vector3 GetMoveDir()
	{
		return rotationReference.rotation * LocalInputManager.currentControls.moveDir;
	}

	virtual protected Quaternion GetLookRot()
	{
		return rotationReference.rotation;
	}

	//called if isLocalPlayer == true and has authority (host player)
	protected void LocalStateUpdate()
	{
		moveDir = GetMoveDir();
		lookRot = GetLookRot();

		sprint = LocalInputManager.currentControls.sprint;
		crouch = LocalInputManager.currentControls.crouch;
		jump = LocalInputManager.currentControls.jump;
		use = LocalInputManager.currentControls.use;
		reload = LocalInputManager.currentControls.reload;
		fire1 = LocalInputManager.currentControls.fire1;
		fire2 = LocalInputManager.currentControls.fire2;
		fire3 = LocalInputManager.currentControls.fire3;
		pause = LocalInputManager.currentControls.pause;
		lookMoveSync = LocalInputManager.currentControls.lookMoveSync;
	}
}
