﻿using UnityEngine;
using System.Collections;

[AddComponentMenu("Camera-Control/Mouse Orbit with zoom")]
public class MouseOrbitImproved : MonoBehaviour {
	
	public Transform target;
	public float distance = 5.0f;
	public float desiredDistance = 5.0f;
	public float xSpeed = 120.0f;
	public float ySpeed = 120.0f;
	
	public float yMinLimit = -20f;
	public float yMaxLimit = 80f;
	
	public float distanceMin = 0.5f;
	public float distanceMax = 15f;

    [Range(0.001f,1.0f)]
	public float smooth = 0.3f;

	float x = 0.0f;
	float y = 0.0f;

    #if UNITY_ANDROID
	Vector3 lastTouch = Vector3.zero;
    #endif

	// Use this for initialization
	void Start () {
		Vector3 angles = transform.eulerAngles;
		x = angles.y;
		y = angles.x;
		
		// Make the rigid body not change rotation
		Rigidbody rb = GetComponent<Rigidbody>();
		if (rb)
			rb.freezeRotation = true;

        //transform.parent = null;
	}

	void OnCameraTargetChanged(Transform target)
	{
		enabled = true;
		this.target = target;
		Debug.Log("Camera target set to "+target);
	}

	
	void FixedUpdate () {

		if (target) {
			if ( target != null )
			{
				//Screen.lockCursor = true;
				//Cursor.visible = false;
				//Screen.showCursor = false;
				Vector3 mousePos = Input.mousePosition;
				mousePos.x = (mousePos.x/Screen.width)-0.5f;
				mousePos.y = (mousePos.y/Screen.height)-0.5f;

#if (UNITY_ANDROID || UNTY_IOS)
				if (mousePos.x > -0.3 && mousePos.y > -0.3){

					if ( lastTouch == Vector3.zero || Input.GetMouseButtonDown(0) )
					{
						lastTouch = mousePos;
					}
					else if (Input.GetMouseButton(0) ){
						x += (mousePos-lastTouch).x * Time.deltaTime*3500.0f * distance;
						y -= (mousePos-lastTouch).y * Time.deltaTime*3500.0f;
						lastTouch = mousePos;
					}
				}
				else
					lastTouch = Vector3.zero;
#else
				if ( Screen.fullScreen || (Input.GetMouseButton(0) && Cursor.visible) ){
					x += Input.GetAxis("Mouse X") * xSpeed * distance * 0.02f;
					y -= Input.GetAxis("Mouse Y") * ySpeed * 0.02f;
				}
#endif

			}
			
			y = ClampAngle(y, yMinLimit, yMaxLimit);
			
			Quaternion rotation = Quaternion.Euler(y, x, 0);

			desiredDistance -= Input.GetAxis("Mouse ScrollWheel")*5;
			desiredDistance = Mathf.Clamp(desiredDistance,distanceMin, distanceMax);
			Ray r = new Ray(target.position, transform.position-target.position );

			float dist = distanceMax;

			LayerMask CollisionLayerMask = LayerMask.GetMask("Default") ;

			bool collide = false;
			//foreach (RaycastHit hit in Physics.SphereCastAll(target.position, 0.3f, Vector3.Normalize(transform.position-target.position)*(desiredDistance+distanceMin)) )
			foreach (RaycastHit hit in Physics.RaycastAll(r, desiredDistance, CollisionLayerMask ) )
			{
				if (hit.collider != target.GetComponent<Collider>() && hit.collider.tag != "Player")
				{
					//Debug.DrawLine(target.position,hit.point,Color.red);
					if (hit.distance < dist){
						dist = hit.distance;
						collide = true;
					}
				}
			}

			distance = collide? Mathf.Clamp(dist-distanceMin,distanceMin, distanceMax) : desiredDistance;
			//distance = desiredDistance;

			Vector3 negDistance = new Vector3(0.0f, 0.0f, -distance);
			Vector3 position = rotation * negDistance + target.position;

			float _smooth = smooth * Time.fixedDeltaTime*60.0f;

			//smooth =  Mathf.Clamp( Time.deltaTime * smooth, 0.1f, 0.99f); //* target.rigidbody.velocity.sqrMagnitude/10.0f

			transform.rotation = Quaternion.Slerp(transform.rotation, rotation, _smooth);

            transform.position = Vector3.Lerp(transform.position, position,  _smooth);
        }
		
	}
	
	public static float ClampAngle(float angle, float min, float max)
	{
		if (angle < -360F)
			angle += 360F;
		if (angle > 360F)
			angle -= 360F;
		return Mathf.Clamp(angle, min, max);
	}

	public Vector3 GetDir(bool Auto)
	{
		return Camera.main.transform.forward;
	}
	
}
