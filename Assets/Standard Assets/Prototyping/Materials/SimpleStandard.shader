﻿Shader "Custom/SimpleStandard" {
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_EmissionMap("Emission", 2D) = "black" {}
		_Glossiness ("Gloss", Range(0,1)) = 0.5
		_Metallic ("Specular", Range(0,1)) = 0.0
	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200
		
		CGPROGRAM
		// Physically based Standard lighting model, and enable shadows on all light types
		#pragma surface surf BlinnPhong

		// Use shader model 3.0 target, to get nicer looking lighting
		#pragma target 3.0

		sampler2D _MainTex,_EmissionMap;

		struct Input {
			float2 uv_MainTex;
		};

		half _Glossiness;
		half _Metallic;
		fixed4 _Color;

		void surf (Input IN, inout SurfaceOutput o) {
			// Albedo comes from a texture tinted by color
			fixed4 c = tex2D (_MainTex, IN.uv_MainTex) * _Color + fixed4(0.1,0.1,0.1,0);
			fixed4 e = tex2D (_EmissionMap, IN.uv_MainTex);
			o.Albedo = c.rgb;
			// Metallic and smoothness come from slider variables
			o.Specular = _Metallic;
			o.Gloss = _Glossiness;
			o.Emission = e;
			o.Alpha = c.a;
		}
		ENDCG
	}
	FallBack "Diffuse"
}
